var express = require('express');
var fs = require('fs');
var http = require('http');
var https = require('https');
var nodeinfo = require('node-info');
var cookieParser = require('cookie-parser')
// var privateKey = fs.readFileSync('nobso.key', 'utf-8');
// var certificate = fs.readFileSync('nobso.crt', 'utf-8');
// var credentials = { key: privateKey, cert: certificate };
var app, httpServer, httpsServer, interval, result, newresult;
var requestedCount = 0;
const axios = require('axios');
var stringify = require('json-stringify-safe');

app = express();
app.use(cookieParser());
app.use(nodeinfo({url: '/node-info'}));

// // event source
// app.use('/stream.js', function(req, res, next) {
//   // log the cookies that are sent by the requester
//   console.log('REQUEST >> ', req.cookies);

//   // make sure that you have a proper content-type
//   res.set({
//     'Content-Type': 'text/event-stream',
//     'Cache-Control': 'no-cache',
//     'Connection': 'keep-alive',
//     // 'Access-Control-Allow-Origin': '*',
//     'Access-Control-Allow-Origin': 'http://localhost:5000',
//     'Access-Control-Allow-Credentials': true,
//     'Acess-Control-Allow-Headers': 'Origin, X-Requested-With, Content-type, Accept'
//   });
//   interval = setInterval(function(){
//     requestedCount ++;
//     fs.readFile('./public/stream-data', function (err, data) {
//     //   if (requestedCount > 5) {
//     //     res.status(204).end();
//     //     return;
//     //   } else {
//         res.write(data);
//     //   }
//     });
//   }, 1000);

//   req.connection.on("close", function() {
//     clearInterval(interval);
//   }, false);
// });

// function sseDemo(req, res) {
//   let messageId = 0;
//   const intervalId = setInterval(() => {
//       res.write(`id: ${messageId}\n`);
//       res.write(`data: Test Message -- ${Date.now()}\n\n`);
//       messageId += 1;
//   }, 1000);

//   req.on('close', () => {
//       clearInterval(intervalId);
//   });
// }

// app.get('/sse-server', (req, res) => {
//   // SSE Setup
//   res.status(200).set({
//     'Connection': 'keep-alive',
//     'Cache-Control': 'no-cache',
//     'Access-Control-Allow-Origin': 'http://localhost:8080',
//     'Access-Control-Allow-Credentials': true,
//     'Content-type': 'text/event-stream',
//   })
//   res.write('\n');
//   // res.write("retry: 10000\n");
//   // res.write("event: connecttime\n");
//   res.write("data: Hello world!\n");
//   // res.write("data: " + (new Date()) + "\n");
//   res.write("\n\n");

//   // sseDemo(req, res);
// });


// app.get('/hello', function (req, res) {
//   req.statusCode(200), set({
//     'Connection': 'keep-alive',
//     'Cache-Control': 'no-cache',
//     'Content-type': 'application/json',
//   })
//   const data = {
//     message: "hello, world!"
//   }
//   setInterval(() => {
//     data.timestamp = Date.now()
//     res.write(JSON.stringify(data))
//   }, 1000)
// });

// app.use('/api', function(req, res, next) {
//   res.setHeader('Content-Type', 'application/json');
//   res.end(JSON.stringify(
//     {"value": 1}, null, 3
//   ));
// });

// const listUsers = async() => {
//   try {
//     const api = await axios.get('https://www.apixu.com/doc/sample_hour_by_hour.json');
//     // console.log(api.headers);
//   } catch (err) {
//     console.log(err);
//   }
// }
// listUsers();

const urlaxios = 'https://www.apixu.com/doc/sample_hour_by_hour.json';
// app.use('/api', function(req, res, next) {
//   axios.get(urlaxios)
//   .then(done => {
//     res.setHeader('Content-Type', 'application/json');
//     res.end(JSON.parse(stringify(done.data)));
//     // res.end(JSON.stringify(done.data));
//     // res.end(JSON.stringify(done.headers));
//     // console.log(data.headers);
//   })
//   .catch(err => {
//     console.log(err)
//   });
// });
const url = "http://localhost/phpMSS/phpPortail/api/apiJson.php";
app.get('/sse-api', function(req, res) {
  res.status(200).set({
    'Connection': 'keep-alive',
    'Cache-Control': 'no-cache',
    'Access-Control-Allow-Origin': 'http://localhost',
    'Access-Control-Allow-Credentials': true,
    'Content-type': 'text/event-stream',
  });
  interval = setInterval(function(){
    axios.get(url)
    .then(done => {
      // res.setHeader('Content-Type', 'application/json');
      // result = JSON.parse(stringify(done.data));
      newresult = JSON.stringify(done.data);
      // res.end(result);
      // res.end(JSON.stringify(done.headers));
      // console.log(data.headers);
      // console.log(resultat);
    })
    .catch(err => {
      // console.log(err)
    });
    // res.write(`data: ${result}\n\n`)
    // res.write(result);
    res.write("data: " + newresult + "\n\n");
  }, 1000);
  // req.connection.on("close", function() {
  //   clearInterval(interval);
  // }, false);
});

// // Consommer SSE dans Nodejs
// http.get({
//   agent: false,
//   path: '/sse-api',
//   hostname: 'localhost:4000',
// }, (res) => {
//   res.on('data', data => {
//     console.log(data.toString())
//   })
// })

//serving static files from the public directory
app.use('/', function(req, res, next) {
//   res.setHeader('Set-Cookie', 'name=example; Domain=.example.com; Path=/;');
  res.setHeader('Set-Cookie', 'name=example; Path=/;');
  console.log('REQUEST >>>', req.method, req.originalUrl);
  next();
}, express.static(__dirname + '/public'));

// control for favicon
app.use(function (q, r) { 
      if (q.url === '/favicon.ico') {
      r.writeHead(200, {'Content-Type': 'image/x-icon'} );
      r.end();
      console.log('favicon requested');
      return;
    }
});

// create a HHT server
httpServer = http.createServer(app);
httpServer.listen(4000, function() {
  console.log('HTTP SERVER is running on PORT 4000');
});

// create a HTTP server
// httpsServer = https.createServer(credentials, app);
// httpsServer.listen(4443, function() {
//     console.log('HTTPS SERVER is running on PORT 4443');
// });