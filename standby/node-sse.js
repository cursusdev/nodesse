var http = require('http');
var fs = require('fs');
var history = [], connections = [];


http.createServer(function(req, res) {
  debugHeaders(req);

  if (req.headers.accept && req.headers.accept == 'text/event-stream') {
    if (req.url == '/events') {
      sendSSE(req, res);
    } else {
      res.writeHead(404);
      res.end();
    }
  } else {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(fs.readFileSync(__dirname + '/sse-node.html'));
    res.end();
  }
}).listen(8000);

function sendSSE(req, res) {
  res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
  });

  var id = (new Date()).toLocaleTimeString();

  setInterval(function() {
    constructSSE(res, id, (new Date()).toLocaleTimeString());
  }, 5000);

  constructSSE(res, id, (new Date()).toLocaleTimeString());
  //res.end();
}

  // app.get('/stats', function (req, res, next){
  //   if (req.headers.accept == 'text/event-stream') {
  //     res.writeHead(200, {
  //       'Content-Type': 'text/event-stream',
  //       'Cache-Control': 'no-cache',
  //       'Connection': 'keep-alive'
  //     });
  
  //     if (req.headers['x-requested-with'] == 'XMLHttpRequest') {
  //       res.xhr = null;
  //     }
  //     if (req.headers['last-event-id']) {
  //       var id = parseIt(req.headers['last-event-id']);
  //       for (var i = 0; i < history.length; i++) {
  //         if (history[id].id >= id) {
  //           sendSSE(req, res);
  //         }
  //       }
  //     } else {
  //       res.write('id\n\n');
  //     }
  //     connections.push(res);
  //     broadcast('Connections', connections.length);
  //     req.on('close', function() {
  //       removeConnection(res);
  //     });
    
  //   } else {
  //     res.writeHead(302, { location: "/index.html" });
  //     res.end();
  //   }
  // });

//   app.get('/index.html', (req, res) => {
//     // SSE Setup
//     res.writeHead(200, {
//         'Content-Type': 'text/html',
//         'Cache-Control': 'no-cache',
//         'Connection': 'keep-alive',
//     });
//     res.write('\n');
//     res.end(content);
// });

function constructSSE(res, id, data) {
  res.write('id: ' + id + '\n');
  res.write("data: " + data + '\n\n');
}

function debugHeaders(req) {
  console.log('URL: ' + req.url);
  for (var key in req.headers) {
    console.log(key + ': ' + req.headers[key]);
  }
  console.log('\n\n');
}