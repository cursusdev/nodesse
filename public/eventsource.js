var message = document.getElementById('message');
message.innerHTML = '';
var connectionStatus = document.getElementById('status');
connectionStatus.innerHTML = '';
var source;

function makeConnection() {
    source = new EventSource('http://localhost:4000/stream.js', { withCredentials: true });
    source.onopen = function(e) {
        console.log('the connection is open');
        connectionStatus.innerHTML = 'the connection is open';
    };
    // source.onmessage = function(e) {
    //     console.log(e.data);
    // };
    source.addEventListener('message', function(e) {
        console.log(e.data);
        var data = e.data.split('\n');
        console.log(data[0]);
        console.log('<>');
        console.log(data[1]);
    });
    source.addEventListener('welcome', function(e) {
        console.log('welcome event data: ', e.data);
        message.innerHTML = `welcome event data: ${e.data}`;
    });
    source.onerror = function(e) {
        console.log('oh.. there is an error buddy');
        connectionStatus.innerHTML = 'the connection is closed';
    };
}

if(!!window.EventSource) {
    // makeConnection();
} else {
    // sorry! buddy, your browser doesn't support EventSource feature
}